const path = require("path");
const coppier = require("copy-webpack-plugin");
const cleaner = require("clean-webpack-plugin");

module.exports = {
    entry: "./src/index.js",
    mode:"development",
    devServer: {
        contentBase: "./dist"
    },
    plugins: [
        new cleaner(["./dist/"]),
        new coppier([
            { from: "./src/static/index.html", to: "./" },
            { from: "./src/static/phaser.min.js", to: "./" },
            { from: "./assets", to: "./assets" }
        ]) //relative to output
    ],
    module: {
        rules: [
            {
                test: /\.css$/,
                use: [
                    "style-loader",
                    "css-loader"
                ]
            },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader"
                }
            }
        ]
    },
    output: {
        filename: "bundle.js",
        path: path.resolve(__dirname, "dist")
    }
};