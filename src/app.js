/**
 * The main app module. The main init. The one to match them all. 
 */
import GameManagerScene from "./scenes/GameManagerScene";

let gameManagerConfig = {
    key: "gameManager",
    active: true,
};

let gameManager = new GameManagerScene(gameManagerConfig);

let scenes=[
    gameManager //First scene will be the starter
];
let phaserConfig = {
    title: "The One to Match them All",
    type: Phaser.WEBGL,
    width: window.innerWidth,
    height: window.innerHeight,
    scaleMode:0,
    scene:scenes
};
let game = new Phaser.Game(phaserConfig);




