/** 
 * Tile Class. It might be a gem but also wall, bomb, etc..
 * 
 * To dos
 * Emmiter on destroy
 * Sounds
*/
class Tile extends Phaser.GameObjects.Sprite {
    constructor(data) {

        super(data.scene);
        this.tileData = {};
        this.tileData.type = Math.floor(Math.random() * Math.floor(data.gemTypes));
        //this.tileData.type = data.id;
        this.tileData.color;
        this.tileData.colorWeight;
        this.tileData.columnId = data.columnId;
        this.tileData.id = data.id;
        this.tileData.scene = data.scene;
        this.tileData.poped = false;
        this.tileData.moved = false;
        this.gameManager = data.scene;
        this.sprite;
        this.name = data.id;

    }

    preload() {

    }

    initialize() {
        this.sprite = this.gameManager.add.sprite(100 + 50 * this.tileData.columnId, 100 + 50 * this.tileData.id, this.tileData.type).setInteractive();
        this.sprite.aParent = this;

        /*this.sprite.on("pointerup", function(e)
        {
            console.log('in tile ', this);
            //ctx.helper.playClickSfx(ctx);
            //callback.call(ctx);
        });
        */

    }

    pop(delayId) {
        //console.log("poping power over 9000 " + delayId);
        /*        this.gameManager.tweens.add({
                    targets: this.sprite,
                    scaleX: 0,
                    scaleY: 0,
                    angle:180,
                    ease: "Sine.easeInOut",
                    duration: 500,
                    //delay: i * 50,
                    delay: delayId*50,
                    repeat: 0,
                    yoyo: false,
                    repeatDelay: 500,
                    onComplete: this.onCompleteHandler,
                    onCompleteParams: [ this.gameManager ]
                });
        */
        
        this.gameManager.tweens.timeline({
            tweens: [{
                targets: this.sprite,
                scaleX: 1.2,
                scaleY: 1.2,
                angle: -20,
                ease: "Sine.easeInOut",
                duration: 50,
                //delay: i * 50,
                delay: delayId * 50,
                repeat: 0,
                yoyo: false,
                repeatDelay: 500,
                onComplete: this.onPopStartingHandler,
                onCompleteParams: [this.gameManager]
            },
            {
                targets: this.sprite,
                scaleX: 0.1,
                scaleY: 0.1,
                angle: 180,
                ease: "Sine.easeOut",
                duration: 500,
                //delay: i * 50,
                delay: 0,
                repeat: 0,
                yoyo: false,
                repeatDelay: 500,
                onComplete: this.onPopCompleteHandler,
                onCompleteParams: [this.gameManager, this.tileData.columnId, this.tileData.id]
            }]
        });
    }

    moveTo(fromPosition, toPosition) {
        this.gameManager.tweens.add({
            targets: this.sprite,
            y: this.sprite.y + this.sprite.height * (toPosition - fromPosition),
            duration: 500,
            ease: "Bounce.easeOut",
            onComplete: this.onMoveCompleteHandler,
            onCompleteParams: [this.gameManager]
            /*props: {
                x: { value: 800, duration: 5000, ease: 'Linear' },
                y: { value: 100, duration: 1000, ease: 'Bounce.easeInOut', yoyo: true, delay: 1000 }
            }*/
        });
    }

    reAppear() {
        this.sprite.setTexture(this.tileData.type);
        this.gameManager.tweens.add({
            targets: this.sprite,
            angle: 360,
            scaleX: 1,
            scaleY: 1,
            ease: "Sine.easeInOut",
            duration: 200,
            onComplete: this.onReAppearCompleteHandler,
            onCompleteParams: [this.gameManager]
        });
    }

    wrongMove() {
        console.log("wrong move");
        this.gameManager.sound.playAudioSprite("sfx", "meow");
        this.gameManager.tweens.add({
            targets: this.sprite,
            angle: 45,
            ease: "Sine.easeInOut",
            duration: 100,
            yoyo: true,
        });
    }

    onPopCompleteHandler(tween, targets, gameManager, x, y) {
        gameManager.popedOneCallback(x, y);
    }

    onPopStartingHandler(tween, targets, gameManager) {
        gameManager.sound.playAudioSprite("sfx", "ping");

    }

    onMoveCompleteHandler(tween, targets, gameManager) {
        gameManager.movedOneCallback();
    }


    onReAppearCompleteHandler(tween, targets, gameManager) {
        gameManager.reAppearCallback();
    }


}
export default Tile;