/**
 * The column manager. 
 * Responisble for manipulating the gems like gravity.
 */
import Tile from "./Tile";

class Column {
    constructor(data){
        /**
         *  let columnData = {
                tilesNo:this.gameSettings.lines, 
                columnId:i, 
                scene:this
            };
         */
        this.elementsNo = data.tilesNo;
        this.id = data.columnId;
        this.scene = data.scene;
        this.tiles = [];
        this.gemTypes= data.gemTypes;
    }

    initialize(){
        for (let i = 0; i < this.elementsNo; i++) {
            let tileData = {
                columnId:this.id,
                id:i, 
                scene:this.scene,
                gemTypes:this.gemTypes
            };
            this.tiles.push(new Tile(tileData));
            this.tiles[i].initialize();
        }
    }
}
export default Column;