class MainScene extends Phaser.Scene {
    constructor() {
        super("MainScene");
    }

    init() {

    }
    preload() {

    }

    create() {
        let text = this.add.text(80, 550, "", { font: "16px Courier", fill: "#ffffff" });

        text.setText([
            "Game Title: " /*+ game.config.gameTitle*/
        ]);

        this.add.image(400, 300, "1");

        let particles = this.add.particles("2");

        let emitter = particles.createEmitter({
            speed: 100,
            scale: { start: 1, end: 0 },
            blendMode: "ADD"
        });

        let logo = this.physics.add.image(400, 100, "3");

        logo.setVelocity(100, 200);
        logo.setBounce(1, 1);
        logo.setCollideWorldBounds(true);

        emitter.startFollow(logo);
    }
}

export default MainScene;
