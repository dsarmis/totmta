/**
 * The game manager module. The main logic module. 
 * States : create           (create stuff - phaser scene state) (can go to Initialize)
 *          initialize      (initialize game) (can go to StartRound) 
 *          startRound      (wait for user click, enable clicks.) (can go to AnimateRound)
 *          animateRound    (user clicked, disable clicks, animate stuff) (can go to EndRound)
 *          endRound        (do stuff like decide where to go npw) (can go to StartRound, EndGame.. etc)
 *          endGame         (animate the end) (can go to Init)
 * 
 * Main Pop Mechanism.
 * If a Tile has 2 or more colorWeight then it can pop. 
 * If a Tile pops then all neighbour tiles of same type will pop. 
 * 
 * To Does
 * 
 * Special Tiles (wild, scatter, unbreakable)
 * Check if move exists. (If not, suffle or game over?)
 * 
 * 
 * Gem Types:
 * 0-3 nomral gems
 * 4 wild (matches all)
 * 5 bomb (haven't decide yet what to do)
 * 
 * Known Features (or bugs.. )
 * Wild cannot be used as an initiator, nor as the middle of three.
 * Wrong move animation stucks on many clicks
 */
import Column from "../modules/Column";

class GameManagerScene extends Phaser.Scene {
    constructor(config) {
        super(config);

        this.gameSettings = {
            columns: 7,
            lines: 7,
            gemTypes: 5,
            wildId: 4,
            specialTypesIds: [5] //This gems cannot be randomly created. Only if you are master of the game you will meet them......
        };

        this.roundSettings = {
            tileType: null,
            tileX: null,
            tileY: null,
            tilesPoped: null,
            tilesMoved: null,
            tilesToPop: null,
            tilesToMove: null,
            roundSettings: null,
            tilesPopedPerColumn: [],
            specialTileUsed: null
        };

        this.specialRound = false; // Who knows? (I do..)

        for (let i = 0; i < this.gameSettings.columns; i++) {
            this.roundSettings.tilesPopedPerColumn[i] = 0;
        }
        this.gameState = null;

        /** game elements*/
        this.columns = [];
        this.tiles = []; //probably wont be used. The columns know their elements. 

    }

    /**Scene init (Phaser)*/
    init() { }

    /**Scene preload (Phaser)*/
    preload() {
        this.load.image("0", "./assets/0.png");
        this.load.image("1", "./assets/1.png");
        this.load.image("2", "./assets/2.png");
        this.load.image("3", "./assets/3.png");
        this.load.image("4", "./assets/4.png");
        this.load.image("5", "./assets/5.png");
        //this.load.spritesheet("fire", "./assets/fire1.png", { frameWidth: 32, frameHeight: 48 });

        this.load.audioSprite("sfx", [
            "./assets/fx_mixdown.ogg",
            "./assets/fx_mixdown.mp3"
        ], "./assets/fx_mixdown.json");

    }

    /**Scene create (Phaser)*/
    create() {

        this.createColumns();

        let text = this.add.text(80, 550, "", { font: "16px Courier", fill: "#ffffff" });
        text.setText(["Game Title: The One to Match them All)" /*+ game.config.gameTitle*/]);


        /*  Add emitter on destroy
                let particles = this.add.particles("2");
                let emitter = particles.createEmitter({
                    speed: 100,
                    scale: { start: 1, end: 0 },
                    blendMode: "ADD"
                });
                let logo = this.physics.add.image(400, 100, "2");
                logo.setVelocity(100, 200);
                logo.setBounce(1, 1);
                logo.setCollideWorldBounds(true);
                emitter.startFollow(logo);
        */
        window.onresize = this.onresizeHandler.bind(this);

        this.initialize();
    }

    /**
     * Gameplay 
     */
    initialize() {
        this.gameState = "initialize";
        this.updateTilesWeight();
        this.startRound();
        this.handleClickOnGems();
    }

    startRound() {
        this.gameState = "startRound";
        this.roundSettings = {
            tileType: -1,
            tileX: -1,
            tileY: -1,
            tilesToPop: 0,
            tilesToMove: 0,
            tilesPoped: 0,
            tilesMoved: 0,
            tilesInitialized: 0,
            tilesPopedPerColumn: [],
            specialTileUsed: false
        };

        for (let i = 0; i < this.gameSettings.columns; i++) {
            this.roundSettings.tilesPopedPerColumn[i] = 0;
        }


        this.enableClickOnGems();

    }

    animateRound(tileType, x, y) {
        this.disableClickOnGems();
        this.startPoping(tileType, x, y, 1); // On end of popping, we move the remaining and after that we create some more. 
        this.gameState = "animateRound";
    }

    endRound() {
        this.startRound();
    }

    endGame() {
        // currently never 
    }

    /**
     * Create functions
     */

    createColumns() {
        for (let i = 0; i < this.gameSettings.columns; i++) {
            let columnData = {
                tilesNo: this.gameSettings.lines,
                columnId: i,
                scene: this,
                gemTypes: this.gameSettings.gemTypes
            };
            this.columns.push(new Column(columnData));
            this.columns[i].initialize();
        }

    }

    /**
    * Helper functions
    */
    updateTilesWeight() {
        this.columns.forEach(column => {
            column.tiles.forEach(tile => {
                this.setTileColorWeight(tile);
            });
        });
    }

    setTileColorWeight(tile) {
        let x = tile.tileData.columnId;
        let y = tile.tileData.id;
        let tileType = tile.tileData.type;
        let sameNeighbours = 0;
        let leftNeighbour;
        let rightNeighbour;
        let upNeighbour;
        let downNeighbour;
        //left neighbour
        if (x > 0) {
            leftNeighbour = this.columns[x - 1].tiles[y].tileData;
            if (leftNeighbour.type == tileType || leftNeighbour.type == this.gameSettings.wildId) {
                sameNeighbours++;
            }
        }
        //right neigbour
        if (x < this.columns.length - 1) {
            rightNeighbour = this.columns[x + 1].tiles[y].tileData;
            if (rightNeighbour.type == tileType || rightNeighbour.type == this.gameSettings.wildId) {
                sameNeighbours++;
            }
        }
        //up neighbour
        if (y > 0) {
            upNeighbour = this.columns[x].tiles[y - 1].tileData;
            if (upNeighbour.type == tileType || upNeighbour.type == this.gameSettings.wildId) {
                sameNeighbours++;
            }
        }
        //down neigbour
        if (y < this.columns[0].tiles.length - 1) {
            downNeighbour = this.columns[x].tiles[y + 1].tileData;
            if (downNeighbour.type == tileType || downNeighbour.type == this.gameSettings.wildId) {
                sameNeighbours++;
            }
        }
        tile.tileData.colorWeight = sameNeighbours;
    }

    enableClickOnGems() {
        this.columns.forEach(column => {
            column.tiles.forEach(tile => {
                //tile.sprite.setInteractive();
                tile.sprite.input.enabled = true;
            });
        });

    }

    disableClickOnGems() {
        this.columns.forEach(column => {
            column.tiles.forEach(tile => {
                tile.sprite.input.enabled = false;
            });
        });
    }

    handleClickOnGems() {
        let gameManager = this;
        this.input.on("gameobjectdown", function (pointer, gameObject) {
            let aColorWeight = gameObject.aParent.tileData.colorWeight;
            let aTileType = gameObject.aParent.tileData.type;
            let aX = gameObject.aParent.tileData.columnId;
            let aY = gameObject.aParent.tileData.id;

            if (aTileType == gameManager.gameSettings.specialTypesIds[0]) {
                gameManager.animateRound(aTileType, aX, aY);
            }
            else if (aTileType != 4 && (aColorWeight > 1 || gameManager.tileChainShouldPop(aTileType, aX, aY)))
                gameManager.animateRound(aTileType, aX, aY);
            else {
                gameObject.aParent.wrongMove();
            }

        });

    }

    onresizeHandler() {
        this.sys.game.renderer.resize(window.innerWidth, window.innerHeight, 1.0);
    }

    moveInArray(tilesArray, from, to) {
        tilesArray.splice(to, 0, tilesArray.splice(from, 1)[0]);
    }

    /**
     * Popping functions
     */


    tileChainShouldPop(tileType, x, y) {

        //left neighbour
        if (x > 0)
            if (this.columns[x - 1].tiles[y].tileData.colorWeight > 1 && this.columns[x - 1].tiles[y].tileData.type == tileType)
                return true;
        //right neigbour
        if (x < this.columns.length - 1)
            if (this.columns[x + 1].tiles[y].tileData.colorWeight > 1 && this.columns[x + 1].tiles[y].tileData.type == tileType)
                return true;
        //up neighbour
        if (y > 0)
            if (this.columns[x].tiles[y - 1].tileData.colorWeight > 1 && this.columns[x].tiles[y - 1].tileData.type == tileType)
                return true;
        //down neigbour
        if (y < this.columns[0].tiles.length - 1)
            if (this.columns[x].tiles[y + 1].tileData.colorWeight > 1 && this.columns[x].tiles[y + 1].tileData.type == tileType)
                return true;
        return false;
    }

    startPoping(tileType, x, y, delayId) {
        this.roundSettings.tileType = tileType;
        this.columns[x].tiles[y].tileData.poped = true;
        if (tileType == this.gameSettings.specialTypesIds[0]) {
            this.popNeighbours(tileType, x, y, delayId) // BOMB explosion. Run boy run...
        } else if (tileType < this.gameSettings.gemTypes && tileType > -1) {

            this.popChaining(tileType, x, y, delayId); //pop the same tiles types
        }

        this.columns[x].tiles[y].pop(delayId);
        this.roundSettings.tilesToPop++;


    }

    popedOneCallback(x, y) {
        this.roundSettings.tilesPoped++;
        if (this.roundSettings.tilesPoped == this.roundSettings.tilesToPop) {
            this.updateColumnsArray();
        }
    }

    popChaining(tileType, x, y, delayId) {
        let wildId = this.gameSettings.wildId;
        let leftNeighbour;
        let rightNeighbour;
        let upNeighbour;
        let downNeighbour;
        //left neighbour
        if (x > 0) {
            leftNeighbour = this.columns[x - 1].tiles[y].tileData;
            if ((leftNeighbour.type == tileType || leftNeighbour.type == wildId) && leftNeighbour.poped == false)
                this.startPoping(tileType, x - 1, y, ++delayId);
        }
        //right neigbour
        if (x < this.columns.length - 1) {
            rightNeighbour = this.columns[x + 1].tiles[y].tileData;
            if ((rightNeighbour.type == tileType || rightNeighbour.type == wildId) && rightNeighbour.poped == false)
                this.startPoping(tileType, x + 1, y, ++delayId);
        }
        //up neighbour
        if (y > 0) {
            upNeighbour = this.columns[x].tiles[y - 1].tileData;
            if ((upNeighbour.type == tileType || upNeighbour.type == wildId) && upNeighbour.poped == false)
                this.startPoping(tileType, x, y - 1, ++delayId);
        }
        //down neigbour
        if (y < this.columns[0].tiles.length - 1) {
            downNeighbour = this.columns[x].tiles[y + 1].tileData;
            if ((downNeighbour.type == tileType || downNeighbour.type == wildId) && downNeighbour.poped == false)
                this.startPoping(tileType, x, y + 1, ++delayId);
        }
    }

    popNeighbours(tileType, x, y, delayId) {
        tileType = -1;
        //left neighbour
        if (x > 0) {
            this.startPoping(tileType, x - 1, y, ++delayId);
            // left up neighbour
            if (y > 0) {
                this.startPoping(tileType, x - 1, y - 1, ++delayId);
            }
            //left down neigbour
            if (y < this.columns[0].tiles.length - 1) {
                this.startPoping(tileType, x - 1, y + 1, ++delayId);
            }
        }
        //right neigbour
        if (x < this.columns.length - 1) {
            this.startPoping(tileType, x + 1, y, ++delayId);
            // right up neighbour
            if (y > 0) {
                this.startPoping(tileType, x + 1, y - 1, ++delayId);
            }
            // right down neigbour
            if (y < this.columns[0].tiles.length - 1) {
                this.startPoping(tileType, x + 1, y + 1, ++delayId);
            }
        }
        //up neighbour
        if (y > 0) {
            this.startPoping(tileType, x, y - 1, ++delayId);
        }
        //down neigbour
        if (y < this.columns[0].tiles.length - 1) {
            this.startPoping(tileType, x, y + 1, ++delayId);
        }
    }


    /**
     *  Moving functions
     */

    // Move the poped tiles to top of array
    updateColumnsArray() {
        let gameManager = this;
        this.columns.forEach(column => {
            column.tiles.forEach(function (tile, index) {
                if (tile.tileData.poped == true) {
                    //tile.tileData.poped = false;
                    tile.tileData.moved = true;
                    gameManager.moveInArray(gameManager.columns[tile.tileData.columnId].tiles, tile.tileData.id, 0);
                }
            });
        });

        this.updateColumnsVisal();
    }

    updateColumnsVisal() {
        let gameManager = this;
        this.columns.forEach(column => {
            column.tiles.forEach(function (tile, index) {
                if (tile.name != index) {
                    gameManager.roundSettings.tilesToMove++;
                    tile.moveTo(tile.name, index);
                }
            });
        });

        let timedEvent = gameManager.time.addEvent({ delay: 1000, callback: gameManager.updateTileData, callbackScope: gameManager, repeat: 0, startAt: 0 });
        //setTimeout(gameManager.updateTileData, 1000); 

    }

    movedOneCallback() {
        /*this.roundSettings.tilesMoved++;
        if (this.roundSettings.tilesMoved == this.roundSettings.tilesToMove) {
            this.updateTileData();
        }
        */
    }

    /**
     * Repopulate tiles functions
     */

    //update tile data (new position, new type)
    updateTileData() {
        let gameManager = this;
        this.columns.forEach(column => {
            column.tiles.forEach(function (tile, index) {
                gameManager.initializeTileDataBasedOnPosition(tile, index);
            });
        });
    }

    initializeTileDataBasedOnPosition(tile, y) {
        tile.tileData.id = y;
        tile.name = y;
        tile.tileData.moved == false;
        if (tile.tileData.poped == true) {
            tile.tileData.poped == false;
            if (this.roundSettings.tilesPoped > 10 &&
                this.roundSettings.specialTileUsed == false &&
                3 < Math.floor(Math.random() * Math.floor(this.gameSettings.gemTypes))) {
                this.roundSettings.specialTileUsed = true;
                tile.tileData.type = this.gameSettings.specialTypesIds[0];
            } else {
                tile.tileData.type = Math.floor(Math.random() * Math.floor(this.gameSettings.gemTypes));
            }
            tile.reAppear();
        }
    }

    initializeTilePopAndMove() {
        this.columns.forEach(column => {
            column.tiles.forEach(function (tile, index) {
                tile.tileData.poped = false;
                tile.tileData.moved = false;
            });
        });
    }

    reAppearCallback() {
        this.roundSettings.tilesInitialized++;
        if (this.roundSettings.tilesInitialized == this.roundSettings.tilesToPop) {
            this.updateTilesWeight();
            this.initializeTilePopAndMove();
            this.endRound();
        }
    }


}
/**
 * Disable logs
 */
//console.log = function() {}

export default GameManagerScene;
