class PreloadScene extends Phaser.Scene {
    constructor(config) {
        console.log("Preload constructor" ,config);
        config.key ="PreloadScene"; 
        super(config);
    }

    init(data) {
        console.log("Preload init" ,data);
    }
    preload(data) {
        console.log("Preload preload" ,data);
        this.load.image("1", "./assets/1.png");
        this.load.image("2", "./assets/2.png");
        this.load.image("3", "./assets/3.png");
        this.load.spritesheet("fire", "./assets/fire1.png", { frameWidth: 32, frameHeight: 48 });
    }

    create(data) {
        console.log("Preload create" ,data);

        this.scene.start("MainScene");
    }
}

export default PreloadScene;
